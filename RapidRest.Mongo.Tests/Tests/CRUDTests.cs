﻿using RapidRest.Paging;
using RapidRest.Mongo;
using System;
using MongoDB.Driver;
using NSubstitute;
using Xunit;
using MongoDB.Bson;
using System.Diagnostics;
using System.Linq;
using NSubstitute.Core;

namespace RapidRest.Mongo.Tests
{
    public class CRUDTests 
    {
        [Fact]
        public void Save() 
        {
            var mongoCollection = NSubstitute.Substitute.For<IMongoCollection<Thing>>();

            IPagedRepository<Thing> mongoRepository = new MongoDataRepository<Thing>(mongoCollection, new RequestContext());

            string id = Guid.NewGuid().ToString();
            var document = new Thing {
                Id = id,
                Name = "John Smith",
                FavouriteColour = "Red"
            };

            mongoRepository.Save(id, document);
            mongoCollection.Received().ReplaceOne(Arg.Any<FilterDefinition<Thing>>(), document, Arg.Any<UpdateOptions>());
        }
    }

    public class Thing {
        public string Id { get; set; } 
        public string Name { get; set; } 
        public string Owner { get; set; } 
        public string FavouriteColour { get; set; }
    }
}
