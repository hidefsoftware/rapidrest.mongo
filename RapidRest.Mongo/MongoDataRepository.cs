using System.Collections.Generic;
using MongoDB.Driver;
using RapidRest.Paging;
using System.Reflection;

namespace RapidRest.Mongo
{
    public class MongoDataRepository<T> : IPagedRepository<T>
    {
        public static MongoDataRepository<T> Create(string connectionString, RequestContext context) 
        {
            var client = new MongoClient(connectionString);
            var database = client.GetDatabase("default");
            var collection = database.GetCollection<T>(typeof(T).Name);
            return new MongoDataRepository<T>(collection, context);
        }

        private readonly IMongoCollection<T> _collection;
        private readonly string _scopeKey;
        private readonly string _scopeValue;
        private FilterDefinition<T> _scopeFilter;
        private readonly string IdFieldName;

        public MongoDataRepository(IMongoCollection<T> collection, RequestContext context)
        {
            _collection = collection;
            _scopeKey = context.ScopeField;
            _scopeValue = context.ScopeClaim?.Value;
            _scopeFilter = context.ScopeField != null ? new FilterDefinitionBuilder<T>().Eq(_scopeKey, _scopeValue) : FilterDefinition<T>.Empty;
            IdFieldName = DiscoveryService.DiscoverIdFieldName(typeof(T));
        }

        public T Delete(string id)
        {
            return _collection.FindOneAndDelete(new FilterDefinitionBuilder<T>().Eq("Id", id) & _scopeFilter);
        }

        public IEnumerable<T> FindBy(string key, object value)
        {
            return _collection.Find(new FilterDefinitionBuilder<T>().Eq(key, value) & _scopeFilter).ToEnumerable();
        }

        public T FindOneBy(string key, object value)
        {
            return _collection.Find(new FilterDefinitionBuilder<T>().Eq(key, value) & _scopeFilter).SingleOrDefault();
        }

        public T Get(string id)
        {            
            return _collection.Find(new FilterDefinitionBuilder<T>().Eq("Id", id) & _scopeFilter).SingleOrDefault();
        }

        public IEnumerable<T> GetAll()
        {
            return _collection.Find(_scopeFilter).ToEnumerable();
        }

        public IEnumerable<T> GetAll(PagingInfo pagingInfo)
        {
            return _collection.Find(_scopeFilter)
                .Skip(pagingInfo.Page * pagingInfo.PageSize)
                .Limit(pagingInfo.PageSize)
                .ToEnumerable();
        }

        public string Save(string id, T obj)
        {   
            setId(id, obj);
            _collection.ReplaceOne(new FilterDefinitionBuilder<T>().Eq("Id", id) & _scopeFilter, obj, new UpdateOptions {
                IsUpsert = true
            });
            return id;
        }

        private void setId(string id, object obj)
        {
            setField(IdFieldName, id, obj);
        }

        private void setField(string fieldname, string value, object obj)
        {
            typeof(T).GetProperty(fieldname).SetValue(obj, value);
        }
    }
}