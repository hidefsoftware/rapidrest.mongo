using System.Collections.Generic;
using Microsoft.Extensions.DependencyInjection;
using RapidRest.Configuration;
using RapidRest.Paging;
using System.Linq;
using System;
using System.Reflection;

namespace RapidRest.Mongo
{
    public static class IServiceCollectionExtensions
    {
        public static void AddRapidRestMongo(this IServiceCollection services, string connectionString)
        {
            var registrations = (IEnumerable<Registration>) services.BuildServiceProvider().GetService(typeof(IEnumerable<Registration>)); 
            
            foreach ( var r in registrations )
            {               
                var dataRepoType = typeof(IPagedRepository<>).MakeGenericType(r.ResourceType);

                services.Remove(services.Single(s => s.ServiceType.FullName == dataRepoType.FullName));
                
                var repoFactory = (Func<IServiceProvider, object>) typeof(IServiceCollectionExtensions)
                            .GetMethod("factory", BindingFlags.NonPublic | BindingFlags.Static)
                            .MakeGenericMethod(r.ResourceType)
                            .Invoke(null, new object[]{ connectionString});

                services.AddScoped(dataRepoType, 
                            repoFactory);
            }
        }

        private static Func<IServiceProvider, object> factory<T>(string connectionString)
        {
            return (IServiceProvider serviceProvider) => {
                var context = (RequestContext) serviceProvider.GetService(typeof(RequestContext));
                return MongoDataRepository<T>.Create(connectionString, context);
            };
        }
    }
}